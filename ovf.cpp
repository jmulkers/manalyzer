#include"ovf.h"
#include<sstream>
#include<iostream>
#include<math.h>
#include<stdlib.h>

// Helper functions
bool parseKeyLine(const std::string& line, std::string& key, std::string& value);
bool parseDataLine(const std::string& line, double& mx, double& my, double& mz);

ovf::ovf(std::string filename) : mx(0), my(0), mz(0), shape(0) {
    ovf::readOVFfile(filename);
}

ovf::ovf() {
    N = 0;
    this->modified();
}

void ovf::readOVFfile(const std::string filename){
    std::ifstream file(filename,std::ios::binary);
    std::string line, key, value;
    if (file.is_open()) {
	while (getline( file, line ))
	   if ( parseKeyLine(line,key,value) )
		if ( key == "Begin"   && value == "Segment" )
		    readSegment(file);
	file.close();
    }
    ovf::setShape();
    this->modified();
}

void ovf::readSegment(std::ifstream& file){
    std::string line, key, value;
    while ( getline (file,line) ){
        if ( parseKeyLine(line,key,value) ){
            if ( key == "End"   && value == "Segment" ) break;
            if ( key == "Begin" && value == "Header" ) readHeader(file);
            if ( key == "Begin" && value == "Data Text" ) readDataText(file);
            if ( key == "Begin" && value == "Data Binary 4" ) readDataBinary(file);
        }
    }
}

void ovf::readHeader(std::ifstream& file){
    std::string line, key, value;
    while ( getline (file,line) ){
        if ( parseKeyLine(line,key,value) ) {
            if ( key == "End" && value == "Header" ) break;
            setVar(key,value);
        }
    }
    N = nodes.x*nodes.y*nodes.z;
    mx.reserve(N);
    my.reserve(N);
    mz.reserve(N);
}

void ovf::readDataText(std::ifstream& file){
    std::string line, key, value;
    double _mx, _my, _mz;
    for (int i=0; i<N; i++) {
	getline(file,line);
	parseDataLine(line,_mx,_my,_mz);
	mx.push_back(_mx);
	my.push_back(_my);
	mz.push_back(_mz);
    }
    if ( getline(file,line) )
	if ( parseKeyLine(line,key,value))
	    if ( key != "End" || value != "Data Text" )
	        std::cout << "problem occured during reading data" << std::endl;
}

void ovf::readDataBinary(std::ifstream& file){
    char k[4];
    file.read(k,4);
    float x, y, z;
    for (int i=0; i<N; i++) {
        file.read((char*)&x, 4);
        file.read((char*)&y, 4);
        file.read((char*)&z, 4);
	mx.push_back((double)x);
	my.push_back((double)y);
	mz.push_back((double)z);
    }
    std::string line, key, value;
    if ( getline(file,line) )
	if ( parseKeyLine(line,key,value))
	    if ( key != "End" || value != "Data Binary 4" )
	        std::cout << "problem occured during reading data" << std::endl;
}

void ovf::modified() const{
    updownCalculated = false;
    updown.reserve(N);
    tcCalculated = false;
    tc = 0.;
    domainsLabeled = false;
    nDomains = 0;
    domainSizesCalculated = false;
    domainLabel.clear();
    domainSize.clear();
    domainTouchingChecked = false;
    touchingDomainPairs.clear();
    mAngleCalculated = false;
    mAngle = 0.;
}

bool ovf::setVar(const std::string  key, const std::string value){
    if         ( key == "Desc"             ){ desc = value ;                       return true;
    } else if  ( key == "Title"            ){ title = value;                       return true;
    } else if  ( key == "meshtype"         ){ meshtype = value;                    return true;
    } else if  ( key == "meshunit"         ){ meshunit = value;                    return true;
    } else if  ( key == "xbase"            ){ base.x = std::stod(value);           return true;
    } else if  ( key == "ybase"            ){ base.y = std::stod(value);           return true;
    } else if  ( key == "zbase"            ){ base.z = std::stod(value);           return true;
    } else if  ( key == "xstepsize"        ){ stepsize.x = std::stod(value);       return true;
    } else if  ( key == "ystepsize"        ){ stepsize.y = std::stod(value);       return true;
    } else if  ( key == "zstepsize"        ){ stepsize.z = std::stod(value);       return true;
    } else if  ( key == "xmin"             ){ min.x = std::stod(value);            return true;
    } else if  ( key == "ymin"             ){ min.y = std::stod(value);            return true;
    } else if  ( key == "zmin"             ){ min.z = std::stod(value);            return true;
    } else if  ( key == "xmax"             ){ max.x = std::stod(value);            return true;
    } else if  ( key == "ymax"             ){ max.y = std::stod(value);            return true;
    } else if  ( key == "zmax"             ){ max.z = std::stod(value);            return true;
    } else if  ( key == "xnodes"           ){ nodes.x = std::stoi(value);          return true;
    } else if  ( key == "ynodes"           ){ nodes.y = std::stoi(value);          return true;
    } else if  ( key == "znodes"           ){ nodes.z = std::stoi(value);          return true;
    } else if  ( key == "ValueRangeMinMag" ){ valueRangeMinMag = std::stod(value); return true;
    } else if  ( key == "ValueRangeMaxMag" ){ valueRangeMaxMag = std::stod(value); return true;
    } else if  ( key == "valueunit"        ){ valueunit = value;                   return true;
    } else if  ( key == "valuemultiplier"  ){ valueMultiplier = std::stod(value);  return true;
    } else return false;
}

void ovf::printInfo() const{
    std::cout << "Title: " << title << std::endl;
    std::cout << "Description: " << desc << std::endl;
}

double ovf::topCharge() const{

    if (tcCalculated) return tc;

    if (nodes.z > 1){
	std::cout << "The system has 3 dimensions (should be 2)" << std::endl;
	return 0.;
    }

    tc = 0;

    for (int iy=0; iy<nodes.y; iy++){
    for (int ix=0; ix<nodes.x; ix++){

	int i  = ix + iy*nodes.x;

	double mxdx = 0.;
	double mydx = 0.;
	double mzdx = 0.;
	double mxdy = 0.;
	double mydy = 0.;
	double mzdy = 0.;

	if (inShape(ix,iy,0)) {

	    bool left  = inShape(ix-1,iy,0);
	    bool right = inShape(ix+1,iy,0);
	    bool up    = inShape(ix,iy+1,0);
	    bool down  = inShape(ix,iy-1,0);
	    int il = ix-1 + iy*nodes.x;
	    int ir = ix+1 + iy*nodes.x;
	    int iu = ix + (iy+1)*nodes.x;
	    int id = ix + (iy-1)*nodes.x;

	    //dx
	    if ( left && right ){
		mxdx = (mx[ir] - mx[il])/2;
		mydx = (my[ir] - my[il])/2;
		mzdx = (mz[ir] - mz[il])/2;
	    } else if ( left && !right ){
		mxdx = mx[i] - mx[il];
		mydx = my[i] - my[il];
		mzdx = mz[i] - mz[il];
	    } else if ( !left && right ){
		mxdx = mx[ir] - mx[i];
		mydx = my[ir] - my[i];
		mzdx = mz[ir] - mz[i];
	    }

	    //dy
	    if ( down && up ){
		mxdy = (mx[iu] - mx[id])/2;
		mydy = (my[iu] - my[id])/2;
		mzdy = (mz[iu] - mz[id])/2;
	    } else if ( down && !up ){
		mxdy = mx[i] - mx[id];
		mydy = my[i] - my[id];
		mzdy = mz[i] - mz[id];
	    } else if ( !down && up ){
		mxdy = mx[iu] - mx[i];
		mydy = my[iu] - my[i];
		mzdy = mz[iu] - mz[i];
	    }

	}

	tc += mx[i]*(mydx*mzdy-mzdx*mydy)
	    + my[i]*(mzdx*mxdy-mxdx*mzdy)
	    + mz[i]*(mxdx*mydy-mydx*mxdy);

    }}
    tc /= (4*M_PI);
    tcCalculated = true;
    return tc;
}

double ovf::topChargeDensity() const {
    if (nodes.z > 1){
	std::cout << "The system has 3 dimensions (should be 2)" << std::endl;
	return 0.;
    }
    double Area = (max.x-min.x)*(max.y-min.y);
    return this->topCharge()/Area;
}

void ovf::setShape(){
    shape.resize(N,false);
    for (int i = 0; i<N; i++){
	if (mx[i]*mx[i]+my[i]*my[i]+mz[i]*mz[i]>0.1)
	    shape[i] = true;
    }
}

bool ovf::inShape(int ix, int iy, int iz) const {
    if ( ix >= 0 && ix < nodes.x &&
	 iy >= 0 && iy < nodes.y &&
	 iz >= 0 && iz < nodes.z    ){
	return shape[ix+nodes.x*iy+nodes.x*nodes.y*iz];
    } else {
	return false;
    }
}

ovf ovf::iso(double l, double u) const {
    ovf o = ovf(*this);
    for (int i = 0; i<N; i++){
	o.mx[i] = 0.;
	o.my[i] = 0.;
	if (o.mz[i] < u && o.mz[i] > l){
	    o.mz[i] = -1;
	} else {
	    o.mz[i] = 1.;
	}
    }
    return o;
}

ovf ovf::contrast() const {
    calcUpdown();
    ovf o = ovf(*this);
    for (int i = 0; i<N; i++){
        o.mx[i] = 0.;
        o.my[i] = 0.;
	if (shape[i]) {
	    if (updown[i])
	        o.mz[i] =  1.;
	    else
	        o.mz[i] = -1.;
	} else {
	    o.mz[i] = 0;
	}
    }
    return o;
}

void ovf::calcUpdown() const {
    if (!updownCalculated){
	for (int i = 0; i<N; i++){
    	    if (mz[i] < 0.){
		updown[i] = false;
    	    } else {
		updown[i] = true;
    	    }
    	}
	updownCalculated=true;
    }
}

icoor ovf::getNodes()    const {return nodes;}
coor  ovf::getMin()      const {return min;}
coor  ovf::getMax()      const {return max;}
coor  ovf::getStepsize() const {return stepsize;}
dvec  ovf::getMx()       const {return mx;}
dvec  ovf::getMy()       const {return my;}
dvec  ovf::getMz()       const {return mz;}
std::vector< dvec > ovf::getM() const {return {mx,my,mz};}

coor ovf::position(int ix, int iy, int iz) const {
    return coor(min.x+ix*stepsize.x , min.y+iy*stepsize.y , min.z+iz*stepsize.z );
}

coor ovf::position(icoor I) const {
    return coor(min.x+I.x*stepsize.x , min.y+I.y*stepsize.y , min.z+I.z*stepsize.z );
}

coor ovf::position(int i) const {
    return ovf::position(ind2subind(i));
}

icoor ovf::ind2subind(int i) const {
    icoor I;
    I.z = i/(nodes.x*nodes.y);
    I.y = i/nodes.x -  I.z*nodes.y;
    I.x = i % nodes.x ;
    return I;
}

int ovf::subind2ind(int ix, int iy, int iz) const {
    return ix + iy*nodes.x + iz*nodes.x*nodes.y;
}

int ovf::subind2ind(icoor i) const {
    return ovf::subind2ind(i.x,i.y,i.z);
}

double ovf::distance(coor pos1, coor pos2) const {
    return sqrt( pow(pos1.x-pos2.x,2) + pow(pos1.y-pos2.y,2) + pow(pos1.z-pos2.z,2));
}

double ovf::distance(int i1, int i2) const {
    coor pos1 = position(i1);
    coor pos2 = position(i2);
    return ovf::distance(pos1,pos2);
}

ddvec ovf::distrRandSampling(double dmax, int nbars,int nsamples) const {
    ovf::calcUpdown();

    double dd = dmax/nbars;
    ivec same(nbars,0);
    ivec different(nbars,0);
    ddvec distr(nbars);

    for (int n=0;n<nsamples;n++){
	int i1 = rand()%N;
	int i2 = rand()%N;
        if (shape[i1] && shape[i2]) {
            int ibar = (int)(distance(i1,i2)/dd);
	    if (ibar < nbars) {
		if ( updown[i1] == updown[i2] )
            	    same[ibar]++;
            	else
		    different[ibar]++;
	    }
        }
    }

    for (int ibar=0; ibar<nbars;ibar++){
	distr[ibar].first = ibar*dd + dd/2;
	distr[ibar].second= (float) same[ibar] / (same[ibar] + different[ibar]);
    }

    return distr;
}

ddvec ovf::distr(double dmax, int nbars,int nsamples) const {
    ovf::calcUpdown();

    double dd = dmax/nbars;
    ivec same(nbars,0);
    ivec different(nbars,0);

    int step = (int) round(N/nsamples);
    for (int i1=0;i1<N;i1+=step){

        if (!shape[i1]) continue;
        icoor ic1 = ind2subind(i1);
        bool updowni1 = updown[i1];

        // create box around sample
        int xa = ic1.x - (int) round(dmax/stepsize.x);
        int xb = ic1.x + (int) round(dmax/stepsize.x);
        int ya = ic1.y - (int) round(dmax/stepsize.y);
        int yb = ic1.y + (int) round(dmax/stepsize.y);
        int za = ic1.z - (int) round(dmax/stepsize.z);
        int zb = ic1.z + (int) round(dmax/stepsize.z);

        // alter the box in order to let it fit in the simulation box
        if (xa < 0) xa = 0;
        if (xb > nodes.x-1) xa = nodes.x-1;
        if (ya < 0) ya = 0;
        if (yb > nodes.y-1) xb = nodes.y-1;
        if (za < 0) za = 0;
        if (zb > nodes.z-1) zb = nodes.z-1;

        // calculate distance between the sample point and the points in the box
        // and see if the orientation is the same
        for (int iz2=za; iz2<=zb; iz2++){
        for (int iy2=ya; iy2<=yb; iy2++){
        for (int ix2=xa; ix2<=xb; ix2++){
            int i2 = subind2ind(ix2,iy2,iz2);
            int ibar = (int)(distance(i1,i2)/dd);
            if (ibar < nbars) {
                if ( updowni1 == updown[i2] )
            	    same[ibar]++;
            	else
                different[ibar]++;
            }
        }}}

    }

    // create the histogram
    ddvec distr(nbars);
    for (int ibar=0; ibar<nbars;ibar++){
	distr[ibar].first = ibar*dd + dd/2;
	distr[ibar].second = (float) same[ibar] / (same[ibar] + different[ibar]);
    }

    return distr;
}

double ovf::typLength(double dmax, int nbars, int nsamples) const {
    ddvec dist = this->distr(dmax,nbars,nsamples);
    int i=0;
    for (i=0;i<dist.size();i++)
        if (dist[i].second < 0.5) break;
    if (i == 0) {
        return dist[0].first;
    } else {
        double x1 = dist[i-1].first;
        double x2 = dist[i].first;
        double y1 = dist[i-1].second;
        double y2 = dist[i].second;
        return x1 + (x2-x1)/(y2-y1)*(0.5-y1);
    }
}

ivec ovf::labelDomains() const {

    if (domainsLabeled) return domainLabel;

    if (nodes.z > 1){
	std::cout << "The system has 3 dimensions (should be 2)" << std::endl;
	return ivec();
    }

    ovf::calcUpdown();

    domainLabel = ivec(N,0);
    int downLabel = 0;
    int upLabel = 0;

    for (int i=0;i<N;i++){

        if (!shape[i]) continue;
        int labelLeft = 0;
        int labelAbove = 0;

        // get label of left and upper neighbor
        if ( (i%nodes.x>0) && (updown[i-1]==updown[i]) )
            labelLeft = domainLabel[i-1];
        if ( (i>= nodes.x) && (updown[i-nodes.x]==updown[i]) )
            labelAbove = domainLabel[i-nodes.x];

        // labeling the current pixel (and relabeling others if nessecary)
        if ( (labelAbove==0) && (labelLeft==0)){
            domainLabel[i] = (updown[i]) ? ++upLabel : --downLabel;
        } else if (labelLeft==0) {
            domainLabel[i] = labelAbove;
        } else if (labelAbove==0) {
            domainLabel[i] = labelLeft;
        } else if (labelAbove==labelLeft) {
            domainLabel[i] = labelLeft;
        } else {
            int chosenLabel = (abs(labelAbove) > abs(labelLeft)) ? labelLeft : labelAbove;
            int removeLabel = (abs(labelAbove) > abs(labelLeft)) ? labelAbove : labelLeft;
            domainLabel[i] = chosenLabel;
            for (int j=0; j<i; j++){
                if (domainLabel[j] == removeLabel)
                    domainLabel[j] = chosenLabel;
            }
        }
    }

    // simplify labels
    upLabel = 0;
    downLabel = 0;
    nDomainsUp = 0;
    nDomainsDown = 0;
    std::map<int,int> remapLabels{{0,0}};
    for ( auto &&l : domainLabel ){
        if (l > upLabel){
            upLabel = l;
            remapLabels[l] = ++nDomainsUp;
        } else if (l < downLabel) {
            downLabel = l;
            remapLabels[l] = -(++nDomainsDown);
        }
        l = remapLabels[l];
    }

    domainsLabeled = true;
    return domainLabel;
}

idmap ovf::domainSizes() const {
    if (domainSizesCalculated) return domainSize;
    this->labelDomains();
    domainSize.clear();
    double cellsize = stepsize.x*stepsize.y;
    for (auto l : domainLabel)
        if (l!=0) domainSize[l]+=cellsize;
    domainSizesCalculated = true;
    return domainSize;
}

int ovf::countDomains(std::string upordown) const {
    this->labelDomains();
    if ( upordown == "down" )           return nDomainsDown;
    else if ( upordown == "up" )        return nDomainsUp;
    else if ( upordown == "upAndDown" ) return nDomainsDown+nDomainsUp;
    else if ( upordown == "downAndUp" ) return nDomainsDown+nDomainsUp;
    else return 0;
}

double ovf::domainDensity(std::string upordown) const {
    double Area = (max.x-min.x)*(max.y-min.y);
    return this->countDomains(upordown)/Area;
}

iiset ovf::touchingDomains() const {

    if (domainTouchingChecked) return touchingDomainPairs;
    touchingDomainPairs.clear();
    this->labelDomains();

    for (int i=0;i<N;i++){

        int labelCenter = domainLabel[i];
        if ( labelCenter==0 ) continue;
        int labelLeft = (i%nodes.x>0) ? domainLabel[i-1] : 0;
        int labelAbove = (i>=nodes.x) ? domainLabel[i-nodes.x] : 0;

        if ( labelLeft < 0 && labelCenter > 0 ) {
            touchingDomainPairs.insert( std::pair<int,int>(labelCenter,labelLeft) );
        } else if ( labelLeft > 0 && labelCenter < 0 ) {
            touchingDomainPairs.insert( std::pair<int,int>(labelLeft,labelCenter) );
        }

        if ( labelAbove < 0 && labelCenter > 0 ) {
            touchingDomainPairs.insert( std::pair<int,int>(labelCenter,labelAbove) );
        } else if ( labelAbove > 0 && labelCenter < 0 ) {
            touchingDomainPairs.insert( std::pair<int,int>(labelAbove,labelCenter) );
        }
    }

    domainTouchingChecked = true;
    return touchingDomainPairs;
}

iimap ovf::nTouchingDomains() const {
    this->touchingDomains();
    iimap ntd;
    for ( auto pair : touchingDomainPairs ){
        ntd[pair.first]++;
        ntd[pair.second]++;
    }
    return ntd;
}

idmap ovf::domainWallLength() const {
    this->labelDomains();
    idmap dwl;
    double dx = stepsize.x;
    double dy = stepsize.y;
    double dxdy = sqrt(dx*dx+dy*dy)/2;
    for (int i=0;i<N;i++){

        int RD = domainLabel[i];
        int LD = (i%nodes.x>0) ? domainLabel[i-1] : 0;
        int RU = (i>=nodes.x) ? domainLabel[i-nodes.x] : 0;
        int LU = (i>=nodes.x && i%nodes.x>0) ? domainLabel[i-nodes.x-1] : 0;

        if ( RD==0 || LD==0 || RU==0 || LU==0 ) continue;
        if ( RD==LD && RD==RU && RD==LU ) continue;

        if        (LU==RU&&LD==RD){
            dwl[LU]+=dy;
            dwl[LD]+=dy;
        } else if (LU==LD&&RU==RD){
            dwl[LU]+=dx;
            dwl[RU]+=dx;
        } else if (LU==LD&&LU==RU){
            dwl[LU]+=dxdy;
            dwl[RD]+=dxdy;
        } else if (RU==RD&&RU==LU){
            dwl[RU]+=dxdy;
            dwl[LD]+=dxdy;
        } else if (LD==LU&&LD==RD){
            dwl[LD]+=dxdy;
            dwl[RU]+=dxdy;
        } else if (RD==RU&&RD==LD){
            dwl[RD]+=dxdy;
            dwl[LU]+=dxdy;
        }
    }
    return dwl;
}

coor ovf::magnetization(int ix, int iy, int iz) const {
    int I = subind2ind(ix,iy,iz);
    return coor(mx[I],my[I],mz[I]);
}

coor ovf::interpolation2d(coor pos) const {
    if (nodes.z > 1){
	std::cout << "The system has 3 dimensions (should be 2)" << std::endl;
	coor mag;
	return mag;
    }
    // bilinear interpolation
    int ix = pos.x/stepsize.x;
    int iy = pos.y/stepsize.y;
    double x = pos.x/stepsize.x - (double)ix; // relative x position
    double y = pos.y/stepsize.y - (double)iy; // relative y position
    int I11 = subind2ind(ix   ,iy   ,0);
    int I12 = subind2ind(ix   ,iy+1 ,0);
    int I21 = subind2ind(ix+1 ,iy   ,0);
    int I22 = subind2ind(ix+1 ,iy+1 ,0);
    double c11 = (1-x)*(1-y);
    double c12 = (1-x)*y;
    double c21 = x*(1-y);
    double c22 = x*y;
    coor mag;
    mag.x = c11*mx[I11] + c12*mx[I12] + c21*mx[I21] + c22*mx[I22] ;
    mag.y = c11*my[I11] + c12*my[I12] + c21*my[I21] + c22*my[I22] ;
    mag.z = c11*mz[I11] + c12*mz[I12] + c21*mz[I21] + c22*mz[I22] ;
    return mag;
}

dcoorvec ovf::pix2pixInterpolation2d(int ixa, int iya, int ixb, int iyb, int N) const {
    dcoorvec magn(N);
    coor posa = position(ixa,iya,0);
    double dx = stepsize.x*(ixb-ixa)/N;
    double dy = stepsize.y*(iyb-iya)/N;
    double dd = sqrt(dx*dx + dy*dy);
    for (int i=0; i<N; i++){
        coor pos;
        pos.x = posa.x + i*dx;
        pos.y = posa.y + i*dy;
        magn[i].first = i*dd;
        magn[i].second = interpolation2d(pos);
    }
    return magn;
}

ovf ovf::range(int ixa, int ixb, int iya, int iyb, int iza, int izb) const {

    ovf ovfNew;

    if (ixa < 0) ixa=0; if (ixa > nodes.x) ixa=nodes.x;
    if (iya < 0) iya=0; if (iya > nodes.y) iya=nodes.y;
    if (iza < 0) iza=0; if (iza > nodes.z) iza=nodes.z;
    if (ixb < 0) ixb=0; if (ixb > nodes.x) ixb=nodes.x;
    if (iyb < 0) iyb=0; if (iyb > nodes.y) iyb=nodes.y;
    if (izb < 0) izb=0; if (izb > nodes.z) izb=nodes.z;

    ovfNew.desc = desc;
    ovfNew.title = title;
    ovfNew.meshtype = meshtype;
    ovfNew.meshunit = meshunit;
    ovfNew.valueunit = valueunit;
    ovfNew.base = base;
    ovfNew.stepsize = stepsize;
    ovfNew.nodes.x = ixb-ixa;
    ovfNew.nodes.y = iyb-iya;
    ovfNew.nodes.z = izb-iza;
    ovfNew.N = ovfNew.nodes.x * ovfNew.nodes.y * ovfNew.nodes.z;
    ovfNew.valueRangeMinMag = valueRangeMinMag;
    ovfNew.valueRangeMaxMag = valueRangeMaxMag;
    ovfNew.valueMultiplier = valueMultiplier;
    ovfNew.mx.reserve(ovfNew.N);
    ovfNew.my.reserve(ovfNew.N);
    ovfNew.mz.reserve(ovfNew.N);
    ovfNew.shape.reserve(ovfNew.N);
    ovfNew.updown.reserve(ovfNew.N);
    ovfNew.min.x = min.x + ixa*stepsize.x;
    ovfNew.min.y = min.y + iya*stepsize.y;
    ovfNew.min.z = min.z + iza*stepsize.z;
    ovfNew.max.x = min.x + ixb*stepsize.x;
    ovfNew.max.y = min.y + iyb*stepsize.y;
    ovfNew.max.z = min.z + izb*stepsize.z;

    for (int iz=iza; iz<izb; iz++){
    for (int iy=iya; iy<iyb; iy++){
    for (int ix=ixa; ix<ixb; ix++){
	ovfNew.mx.push_back(mx[subind2ind(ix,iy,iz)]);
	ovfNew.my.push_back(my[subind2ind(ix,iy,iz)]);
	ovfNew.mz.push_back(mz[subind2ind(ix,iy,iz)]);
    }}}
    ovfNew.setShape();
    return ovfNew;
}

ovf ovf::rangeDist(double xa, double xb, double ya, double yb, double za, double zb) const {
    // int ixa = floor( (xa-xmin)/xstepsize );
    // int ixb = ceil(  (xb-xmin)/xstepsize );
    // int iya = floor( (ya-ymin)/ystepsize );
    // int iyb = ceil(  (yb-ymin)/ystepsize );
    // int iza = floor( (za-zmin)/zstepsize );
    // int izb = ceil(  (zb-zmin)/zstepsize );
    // return ovf::range(ixa,ixb,iya,iyb,iza,izb);
}

double ovf::angle(int i,int j) const {
    return acos(mx[i]*mx[j] + my[i]*my[j] + mz[i]*mz[j])*180.0/M_PI;
}

double ovf::maxAngle() const {
    if (mAngleCalculated) return mAngle;
    mAngle = 0.;
    for (int iz = 0; iz<nodes.z ; iz++ ){
    for (int iy = 0; iy<nodes.y ; iy++ ){
    for (int ix = 0; ix<nodes.x ; ix++ ){
	if (!inShape(ix,iy,iz)) continue;
	int i1 = subind2ind(ix,iy,iz);
	int neighbours[3]; // neighbours of i1
	neighbours[0] = inShape(ix+1,iy,iz)? subind2ind(ix+1,iy,iz) : N+1;
	neighbours[1] = inShape(ix,iy+1,iz)? subind2ind(ix,iy+1,iz) : N+1;
	neighbours[2] = inShape(ix,iy,iz+1)? subind2ind(ix,iy,iz+1) : N+1;
	for ( auto i2 : neighbours ){
	    if (i2>N ) continue;
	    double a = angle(i1,i2);
	    mAngle = a > mAngle ? a : mAngle;
	}
    }}}
    mAngleCalculated = true;
    return mAngle;
}

void ovf::writeOVFfile(const std::string filename) const {
    std::cout << filename << std::endl;

    std::ofstream outfile;
    outfile.open(filename);
    outfile << "# OOMMF: rectangular mesh v1.0"		 << std::endl;
    outfile << "# Segment count: 1"             		 << std::endl;
    outfile << "# Begin: Segment"               		 << std::endl;
    outfile << "# Begin: Header"                		 << std::endl;
    outfile << "# Desc: "             <<   desc                << std::endl;
    outfile << "# Title: "            <<   title               << std::endl;
    outfile << "# meshtype: "         <<   meshtype            << std::endl;
    outfile << "# meshunit: "         <<   meshunit            << std::endl;
    outfile << "# xbase: "            <<   base.x              << std::endl;
    outfile << "# ybase: "            <<   base.y              << std::endl;
    outfile << "# zbase: "            <<   base.z              << std::endl;
    outfile << "# xstepsize: "        <<   stepsize.x          << std::endl;
    outfile << "# ystepsize: "        <<   stepsize.y          << std::endl;
    outfile << "# zstepsize: "        <<   stepsize.z          << std::endl;
    outfile << "# xmin: "             <<   min.x               << std::endl;
    outfile << "# ymin: "             <<   min.y               << std::endl;
    outfile << "# zmin: "             <<   min.z               << std::endl;
    outfile << "# xmax: "             <<   max.x               << std::endl;
    outfile << "# ymax: "             <<   max.y               << std::endl;
    outfile << "# zmax: "             <<   max.z               << std::endl;
    outfile << "# xnodes: "           <<   nodes.x             << std::endl;
    outfile << "# ynodes: "           <<   nodes.y             << std::endl;
    outfile << "# znodes: "           <<   nodes.z             << std::endl;
    outfile << "# ValueRangeMinMag: " <<   valueRangeMinMag    << std::endl;
    outfile << "# ValueRangeMaxMag: " <<   valueRangeMaxMag    << std::endl;
    outfile << "# valueunit: "        <<   valueunit           << std::endl;
    outfile << "# valuemultiplier: "  <<   valueMultiplier     << std::endl;
    outfile << "# End: Header"                                 << std::endl;
    outfile << "# Begin: Data Text"                            << std::endl;
    for (int i=0;i<N;i++) {
	outfile << mx[i] << " " << my[i] << " " << mz[i] << std::endl;
    }
    outfile << "# End: Data Text"                              << std::endl;
    outfile << "# End: Segment"               		 << std::endl;
}

// Implementation of helper functions
//----------------------------------------------------------------------------------

bool parseKeyLine(const std::string& line, std::string& key, std::string& value){
    if ( line.at(0) == '#' && line.at(1) == ' ' ) {
        int i = line.find(':');
        key   = line.substr(2,i-2);
        value = line.substr(i+2);
        return true;
    } else {
        return false;
    }
}

bool parseDataLine(const std::string& line, double& mx, double& my, double& mz){
    if ( line.at(0) != '#' ) {
        std::stringstream str(line);
        str >> mx;
        str >> my;
        str >> mz;
        return true;
    } else {
        return false;
    }
}
