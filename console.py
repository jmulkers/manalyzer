from IPython.qt.console.rich_ipython_widget import RichIPythonWidget
from IPython.qt.inprocess import QtInProcessKernelManager
from IPython.lib import guisupport

class Console(RichIPythonWidget):

    def __init__(self, parent=None):
        RichIPythonWidget.__init__(self, parent)
        self.kernel_manager = QtInProcessKernelManager()
        self.kernel_manager.start_kernel()
        self.kernel_manager.kernel.gui = 'qt4'
        self.kernel_client = self.kernel_manager.client()
        self.kernel_client.start_channels()
        self.user_ns = self.kernel_manager.kernel.shell.user_ns

    def push(self,variables):
        self.kernel_manager.kernel.shell.push(variables)

    def stop(self):
        self.console.kernel_client.stop_channels()
        self.console.kernel_manager.shutdown_kernel()

    #self.exit_requested.connect(stop)
