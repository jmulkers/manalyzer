"""
The table module contains the table class and related functions
"""

import numpy as np
import os
import sys
import copy
import shutil
import util
import ovf
from subprocess import call

class Table(dict):

    """ stores table as a dictionary """

    def __init__(self,dirname=None):
	if dirname!=None:
	    self.read(dirname)

    def __setitem__(self, key, item):
        self.__dict__[key] = item

    def __getitem__(self, key):
        return self.__dict__[key]

    def __repr__(self):
	info = "variables: \n"
	for var in self["_vars"]:
	    info = info + "\t%s (%s)" % (var, self["_units"][var]) + '\n'
        if self["_extvars"]:
	    info = info + "external variables: \n"
	    for var in self["_extvars"]:
	        info = info + "\t%s" % (var) + '\n'
	info = info + "number of entries: %d "  % self.nrows()
	return info

    def __len__(self):
        return len(self.__dict__)

    def __delitem__(self, key):
        del self.__dict__[key]

    def __cmp__(self, dict):
        return cmp(self.__dict__, dict)

    def __contains__(self, item):
        return item in self.__dict__

    def __iter__(self):
        return iter(self.__dict__)

    def __call__(self):
        return self.__dict__

    def __unicode__(self):
        return unicode(repr(self.__dict__))

    def add(self, key, value):
        self.__dict__[key] = value

    def keys(self):
        return self.__dict__.keys()

    def values(self):
        return self.__dict__.values()

    def nrows(self):
        if self.nVars()==0: print "no variables in table"
        else: return len(self[self["_vars"][0]])

    def nVars(self):
        return len(self["_vars"])

    def nExtvars(self):
        return len(self["_extvars"])

#-------------------------------------------------------------------------------------

    def read(self,dirname):

	""" read table.txt located in 'dirname' """

	dirname = os.path.realpath(dirname)

	with open(os.path.join(dirname,'table.txt'),'r') as f:

            header = f.readline().split()
    	    var = header[1::2]
            units = dict(zip(var, [u[1:-1] for u in header[2::2]]))

    	    data = [ [] for x in var ]
    	    for line in f:
                if line[0] == '#': continue
    	        line = line.split()
    	        for j in range(len(var)):
                    data[j].append(util.convertStr(line[j]))

    	self.__dict__.update(dict(zip(var,data)))
    	self["_vars"] = var
    	self["_units"] = units
        self["_extvars"] = []

        # get path of m?????.ovf files
	if ("cnt" in self["_vars"]):
            paths = [ os.path.join(dirname,"m%.6d.ovf"%cnt) for cnt in self["cnt"] ]
	    self.addColumn("path","",paths)

#-------------------------------------------------------------------------------------

    def print2screen(self,keys="all"):

	""" print table in the terminal """

        if keys=="all":
            keys = self["_vars"]
        print '# ',
        for key in keys:
            print key,'('+self["_units"][key]+')','\t',
        print ''
        for i in range(self.nrows()):
            for key in keys:
                print self[key][i], '\t',
            print ''

#-------------------------------------------------------------------------------------

    def deleteRow(self,I):

        """ delete rows with indices in list I """

        for i in I:
            for key in self["_vars"] + self["_extvars"]:
                del self[key][i]

#-------------------------------------------------------------------------------------

    def write(self,dirname,keys="all",copyOVF=True):

        """ write table to file in dirname and copy ovf files if necessary """

        # set variables to copy
        if keys=="all":
            keys = copy.copy(self["_vars"])
        else:
            keys = [key for key in keys if key in self["_vars"]]
        if "path" in keys: keys.remove("path") # path does not need to be printed

        # make directory
	dirname = os.path.realpath(dirname)
	os.mkdir(dirname)

        # copy ovf files
	if copyOVF:
            for i,oldpath in enumerate(self["path"]):
                if os.path.exists(oldpath):
                    newpath = os.path.join(dirname,'m%.6d.ovf'%i)
		    shutil.copyfile(oldpath,newpath)
                    self["path"][i] = newpath
                    self["cnt"][i] = i

        # write table
	f = open(os.path.join(dirname,'table.txt'),'w')
	f.write("# ")
        for key in keys:
            f.write("%s (%s)\t"%(key,self["_units"][key]))
	f.write("\n")
        for i in range(self.nrows()):
            for key in keys:
                f.write(str(self[key][i])+"\t")
            f.write("\n")
	f.close()

#-------------------------------------------------------------------------------------

    def addColumn(self,key,unit,values,overwrite=False):

        """ add column to table """

        if key in self["_vars"] + self["_extvars"]:
            if overwrite:
                self.removeColumn(key)
            else:
                raise Exception("variable name already exist")
        if type(values) is not list:
            values = self.nrows()*[values]
        elif self.nrows() != len(values):
            raise Exception("length of columns does not match")
	self["_vars"].append(key)
    	self["_units"][key] = unit
    	self[key] = values

#-------------------------------------------------------------------------------------

    def addExtvar(self,key,values,overwrite=False):

        """ add external variable """

        if key in self["_vars"] + self["_extvars"]:
            if overwrite:
                self.removeColumn(key)
            else:
                raise Exception("variable name already exist")
        if self.nrows() != len(values):
            raise Exception("length of columns does not match")
	self["_extvars"].append(key)
    	self[key] = values

#-------------------------------------------------------------------------------------

    def removeColumn(self,key):

        """ remove column from table """

        if key in self["_vars"]:
            self["_vars"].remove(key)
            del self[key]
            del self["_units"][key]
        elif key in self["_extvars"]:
            self["_extvars"].remove(key)
            del self[key]
        else:
            raise Exception("variable does not exist")

#-------------------------------------------------------------------------------------

    def sort(self,key):

        """ get the indeces of the sorted table """

        return list(np.argsort(self[key]))

#-------------------------------------------------------------------------------------

    def sub(self,I):

        """ returns a subset of the table """

        table_out = Table()
        for key in self.keys():
            if key[0] == '_' :
                table_out[key] = self[key]
            else:
                table_out[key] = []
                for i in I:
                    table_out[key].append(self[key][i])
        return table_out

#-------------------------------------------------------------------------------------

    def filter(self,boolfunc,keys):

        """ filter out data based on a boolean function on one of the parameters """

        if type(keys) != list: keys = [keys]
        I = []
        for i in range(self.nrows()):
            if boolfunc(*[self[key][i] for key in keys]):
                I.append(i)
        return I

#-------------------------------------------------------------------------------------

    def clusters(self,key,err):

        """ 'clusterizes' the table based on a certain key """

        clusters = []
        for i, v in enumerate(self[key]):
            inCluster = False
            for c in clusters:
                if abs(v-self[key][c[0]])<err:
                    c.append(i)
                    inCluster = True
                    break
            if not inCluster:
                clusters.append([i])
        return clusters

#-------------------------------------------------------------------------------------

    def show(self,keys=None,I=None):

        """ shows every element of the table """

	if keys==None: keys = self["_vars"]
	if I==None: I = range(self.nrows())
        if I==int: I=[I]

        from IPython.display import Image
        from IPython.display import display

	for i in I:
            print "\n-----------------------------------\n"
            ovffile = self["path"][i]
            if os.path.isfile(ovffile):
                call("~/PHD/Tools/mumax3-convert -png " + ovffile,shell=True)
                pngfile = os.path.splitext(ovffile)[0]+".png"
                display(Image(pngfile))
                os.remove(pngfile)
	    for key in keys:
		print key, self[key][i]

#-------------------------------------------------------------------------------------

    def loadOVF(self):

        """ load ovf file as external variable using the path variable """

        self.addExtvar("OVF",[ovf.ovf(path) for path in self["path"]])

#-------------------------------------------------------------------------------------

    def getOVF(self, I=None):

        """ get ovf file using the path variable """

        if "path" not in self["_vars"]:
            raise Exception("ovf filepaths not given")

        if I == None: I = range(self.nrows())

        if type(I) == list:
            if 'OVF' in self['_extvars']:
                return [self["OVF"][i] for i in I]
            else:
                return [ovf.ovf(self["path"][i]) for i in I]
        elif type(I) == int:
            if 'OVF' in self['_extvars']:
                return self["OVF"][I]
            else:
                if os.path.isfile(self["path"][I]):
                    return ovf.ovf(self["path"][I])
                else:
                    raise Exception("File does not exists")

#-------------------------------------------------------------------------------------

    def convertOVF(self,options):

        """ call mumax3-convert in order to convert ovf files """

        if "path" in self["_vars"]:
            for path in self.path:
                call("~/PHD/Tools/mumax3-convert " \
                        + options + " " + path,shell=True)

#-------------------------------------------------------------------------------------

    def calcOVFProperties(self):

        """ calculates multiple ovf properties """

        methods = ["labelDomains","domainSizes","countDomains","domainDensity","domainWallLength"]
        results = dict(zip(methods,[[] for method in methods]))

        for i in range(self.nrows()):
            o = self.getOVF(i)
            for method in methods:
                results[method].append(getattr(o,method)())
            util.progressbar(float(i+1)/self.nrows())
        for method in methods:
            if type(results[method][0]) == float or type(results[method][0]) == int:
                self.addColumn(method,"",results[method])
            else:
                self.addExtvar(method,results[method])

#-------------------------------------------------------------------------------------

    def calcOVF(self,method,*args):

        """ calls ovf method for each config """

        result = []
        for i in range(self.nrows()):
            try:
                o = self.getOVF(i)
            except:
                result.append(np.nan)
            else:
                result.append(getattr(o,method)(*args))
            util.progressbar(float(i+1)/self.nrows())
        return result

#-------------------------------------------------------------------------------------

    def calcTopCharges(self):

	""" calculates the topological charge of every configuration in the table """

        if "tc" not in self["_vars"]:
            self.addColumn("tc","",self.calcOVF('topCharge'))

#-------------------------------------------------------------------------------------

    def calcTopChargeDensities(self):

	""" calculates the topological charge density of every configuration in the table """

        if "tc_dens" not in self["_vars"]:
            self.addColumn("tc_dens","",self.calcOVF('topChargeDensity'))

#-------------------------------------------------------------------------------------

    def calcMaxAngle(self):

        """ calculates the maximal angle of every config """

        if "maxAngle" not in self["_vars"]:
            self.addColumn("maxAngle","",self.calcOVF('maxAngle'))

#-------------------------------------------------------------------------------------

    def calcTypLength(self,dmax,nbars,nsamples):

        """ calculates the typical domain length of every config """

        self.addColumn("typLength","",self.calcOVF('typLength',dmax,nbars,nsamples),True)

#-------------------------------------------------------------------------------------

    def calcNumberDomains(self):

        """ calculates the number of domains in every config """

        if "nDomains" not in self["_vars"]:
            self.addColumn("nDomains","",self.calcOVF('countDomains'))

#-------------------------------------------------------------------------------------

    def calcDomainDensity(self):

        """ calculates the domainDensity in every config """

        if "domainDens" not in self["_vars"]:
            self.addColumn("domainDens","1/m2",self.calcOVF('countDomains'))

#-------------------------------------------------------------------------------------

    def calcDomainSizes(self):

        """ Calculates the sizes of all the domains in each config """

        if "domainSizes" not in self["_extvars"]:
            self.addExtvar("domainSizes",self.calcOVF('domainSizes'))

#-------------------------------------------------------------------------------------

    def getAllDomainSizes(self):
        """ Gives the sizes of all the domains in every config """
        if 'domainSizes' not in self.keys():
            self.calcDomainSizes()
        return np.concatenate( [sizes.values() for sizes in self['domainSizes']] )

#-------------------------------------------------------------------------------------

    def domainRadiusHistogram(self,nbars=20):

        """ Create histogram of the domain radii """

        import matplotlib.pyplot as plt
        A = self.getAllDomainSizes()
        r = 1e9*np.sqrt(A/np.pi)
        import matplotlib.pyplot as plt
        plt.hist(r,nbars)
        plt.show()

#-------------------------------------------------------------------------------------

    def calcDomainWallLengths(self):

        """ Calculates the domain wall length of each domain """

        self.addExtvar("dwLengths",self.calcOVF('domainWallLength'),True)

#-------------------------------------------------------------------------------------

    def calcDomainWallLength(self):

        """ Calculates the domain wall length of each domain """
        dwls = self.calcOVF('domainWallLength')
        dwl = [ sum(x.values()) for x in dwls]
        self.addColumn("dwLength","nm",dwl,True)

#-------------------------------------------------------------------------------------

    def calcRadialDistributions(self,maxd,nbars,nsamples):

        """ Calculates the radial distribution """

        self.addExtvar("radDistr",self.calcOVF('distr',maxd,nbars,nsamples),True)

#-------------------------------------------------------------------------------------

    def plotRadialDistributions(self,maxd,nbars,nsamples):

        """ plot the radial distribution """

        import matplotlib.pyplot as plt
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot([0,maxd],[0.5,0.5])
        plt.xlabel("d (nm)")
        plt.ylabel("p")
        self.addExtvar("radDistr",self.calcOVF('distr',maxd,nbars,nsamples),True)
        for i in range(self.nrows()):
            x,y = zip(*self['radDistr'][i])
            ax.plot(x,y,'.-',label=str(i),picker=2)
        def onpick(event):
            name = event.artist.get_label()
            self.sub([int(name)]).show()
            sys.stdout.flush()
        fig.canvas.mpl_connect('pick_event', onpick)
        plt.show()

#-------------------------------------------------------------------------------------

    def plot(self,xkey,ykey=None,zkey=None):

        """ create interactive plot of data in the table """

        import matplotlib.pyplot as plt
        fig = plt.figure()

        if ykey!=None:
            x = self[xkey]
            y = self[ykey]
            plt.xlabel(xkey+' ('+self["_units"][xkey]+')')
        else:
            ykey = xkey
            y = self[ykey]
            x = range(len(y))
            plt.xlabel("n")
        plt.ylabel(ykey+' ('+self["_units"][ykey]+')')
	if zkey!=None:
	    c = self[zkey]
            zlabel = zkey+' ('+self["_units"][zkey]+')'
	else:
	    c = len(y)*[0]
        ax = fig.add_subplot(111)
        sc = ax.scatter(x,y,c=c,picker=4)
        if zkey!=None:
	    plt.colorbar(sc,label=zlabel)
        plt.xlim([min(x),max(x)])
        def onpick(event):
            index = event.ind[0]
            print index
            self.sub([index]).show()
            sys.stdout.flush()
        fig.canvas.mpl_connect('pick_event', onpick)
        plt.show()

#-------------------------------------------------------------------------------------

    def qtPlot(self):
        from PyQt4.QtGui import QApplication
        from plotter import Plotter
        global app
        app = QApplication.instance()
        if app is None:
            app = QApplication(sys.argv)
        myplotter = Plotter({"table":self})
        myplotter.show()
        app.exit(app.exec_())

    def qtInteractor(self):
        import sip
        sip.setapi('QString', 2)
        sip.setapi('QVariant', 2)
        from PyQt4.QtGui import QApplication
        from tableViewer import TableViewer
        global app
        app = QApplication.instance()
        if app is None:
            app = QApplication(sys.argv)
        myplotter = TableViewer({"table":self})
        myplotter.show()
        app.exit(app.exec_())



#-------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------

def joinTables(tables):

    """ Put tables in a list to a single table """

    varkeys = tables[0]["_vars"]
    extvarkeys = tables[0]["_extvars"]
    for t in tables:
        varkeys = [key for key in varkeys if key in t["_vars"]]
        extvarkeys = [key for key in extvarkeys if key in t["_extvars"]]
    units = {}
    for key in varkeys:
        units[key] = tables[0]["_units"][key]

    tab = Table()
    tab._vars = varkeys
    tab._extvars = extvarkeys
    tab._units = units

    for key in varkeys + extvarkeys:
        tab[key] = []
    for i,t in enumerate(tables):
	for key in varkeys + extvarkeys:
	    tab[key] = tab[key] + t[key]

    return tab

#-------------------------------------------------------------------------------------

def plot(td,xkey,ykey,legend=True):

    """ plot dictionary with tables """

    import matplotlib.pyplot as plt
    import matplotlib.cm as cm
    fig = plt.figure()
    ax = fig.add_subplot(111)
    scatters = []
    plots = []
    colors = cm.rainbow(np.linspace(0, 1, len(td)))
    xmin = float("inf")
    xmax = float("-inf")
    for i,name in enumerate(td):
        scatters.append(ax.scatter(td[name][xkey],td[name][ykey],picker=4,label=name,color=colors[i]))
        #plots.append(ax.plot(td[name][xkey],td[name][ykey],picker=4,label=name,color=colors[i]))
        xminthis = min(td[name][xkey])
        xmaxthis = max(td[name][xkey])
        xmin = xmin if xmin < xminthis else xminthis
        xmax = xmax if xmax > xmaxthis else xmaxthis
    def onpick(event):
        name = event.artist.get_label()
        index = event.ind[0]
        print name, index
        td[name].sub([index]).show()
        sys.stdout.flush()
    plt.xlim([xmin,xmax])
    plt.xlabel(xkey+' ('+td[td.keys()[0]]["_units"][xkey]+')')
    plt.ylabel(ykey+' ('+td[td.keys()[0]]["_units"][ykey]+')')
    if legend: plt.legend()
    fig.canvas.mpl_connect('pick_event', onpick)
    plt.show()

