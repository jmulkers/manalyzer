from PyQt4.QtCore import *
from PyQt4.QtGui import *

import matplotlib
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
from matplotlib.figure import Figure


import plotter_ui
import table

class Plotter(QWidget):

    selectSignal = pyqtSignal(str,int)

    def __init__(self, tabulars=None, parent=None):

        QWidget.__init__(self, parent)

        self.tables = tabulars

        # create plot figure,axes, canvas widget and toolbar widget
        self.fig = Figure((5.0, 4.0))
        self.fig.set_facecolor("#f3f3f3")
        self.axes = self.fig.add_subplot(111)

        # setup most of ui
        self.ui = plotter_ui.Ui_Form()
        self.ui.setupUi(self)

        # add canvas to ui for the plot
        self.ui.canvas = FigureCanvas(self.fig)
        self.ui.canvas.setParent(self.ui.plotFrame)
        self.ui.mpl_toolbar = NavigationToolbar(self.ui.canvas, self)
        vbox = QVBoxLayout()
        vbox.addWidget(self.ui.canvas)
        vbox.addWidget(self.ui.mpl_toolbar)
        self.ui.plotFrame.setLayout(vbox)

        # link pick event to select signal
        def onpick(event):
            tableName = event.artist.get_label()
            self.selectSignal.emit(tableName,event.ind[0])
        self.fig.canvas.mpl_connect('pick_event', onpick)

        # connections
        self.ui.xkeySelector.installEventFilter(self)
        self.ui.ykeySelector.installEventFilter(self)
        self.ui.zkeySelector.installEventFilter(self)
        self.ui.xkeySelector.currentIndexChanged.connect(self.updatePlot)
        self.ui.ykeySelector.currentIndexChanged.connect(self.updatePlot)
        self.ui.zkeySelector.currentIndexChanged.connect(self.updatePlot)

        self.ui.dotsCB = {}
        self.ui.lineCB = {}
        self.ui.colorLE = {}

        if len(self.tables) is not 0:
            self.updatePlotPropper()
            self.updateKeyChoosers()
            self.updatePlot()

    def updatePlotPropper(self,dotsCBstate={},lineCBstate={},colorLEstate={}):

        # get state of the propper before the update
        if not dotsCBstate:
            for name,CB in self.ui.dotsCB.iteritems():
                dotsCBstate[name] = CB.checkState()
        if not lineCBstate:
            for name,CB in self.ui.lineCB.iteritems():
                lineCBstate[name] = CB.checkState()
        if not colorLEstate:
            for name,LE in self.ui.colorLE.iteritems():
                colorLEstate[name] = LE.text()

        # delete all widgets in gridPlotProps
        for i in reversed(range(self.ui.gridPlotProps.count())):
            self.ui.gridPlotProps.itemAt(i).widget().setParent(None)

        # reset some widgets
        colors = ['blue','green','red','cyan','magenta','yellow','black']
        for i,name in enumerate(self.tables.keys()):

            self.ui.dotsCB[name] = QCheckBox("dots")
            if name in dotsCBstate.keys():
                self.ui.dotsCB[name].setChecked(dotsCBstate[name])
            else:
                self.ui.dotsCB[name].setChecked(True)

            self.ui.lineCB[name] = QCheckBox("line")
            if name in lineCBstate.keys():
                self.ui.lineCB[name].setChecked(lineCBstate[name])

            self.ui.colorLE[name] = QLineEdit(colors[i%len(colors)])
            if name in colorLEstate.keys():
                self.ui.colorLE[name].setText(colorLEstate[name])

            self.ui.dotsCB[name].stateChanged.connect(self.updatePlot)
            self.ui.lineCB[name].stateChanged.connect(self.updatePlot)
            self.ui.colorLE[name].returnPressed.connect(self.updatePlot)
            self.ui.gridPlotProps.addWidget(QLabel(name),i,0)
            self.ui.gridPlotProps.addWidget(self.ui.dotsCB[name],i,1)
            self.ui.gridPlotProps.addWidget(self.ui.lineCB[name],i,2)
            self.ui.gridPlotProps.addWidget(self.ui.colorLE[name],i,3)

    def eventFilter(self,source,event):
        if event.type() == QEvent.MouseButtonPress:
            if source in [self.ui.xkeySelector,
                self.ui.ykeySelector,
                self.ui.zkeySelector]:
                    self.updateKeyChoosers()
        return QWidget.eventFilter(self,source,event)

    def updateComboBox(self,comboBox,values):
        comboBox.blockSignals(True)
        currentText = comboBox.currentText()
        comboBox.clear()
        comboBox.addItems(values)
        ind = comboBox.findText(currentText)
        if ind == -1:
            ind = 0
        comboBox.setCurrentIndex(ind)
        comboBox.blockSignals(False)

    def updateKeyChoosers(self):
        vars = self.tables.values()[0]["_vars"]
        for t in self.tables.values():
            vars = [key for key in vars if key in t["_vars"]]
        self.updateComboBox(self.ui.xkeySelector,vars)
        self.updateComboBox(self.ui.ykeySelector,vars)
        self.updateComboBox(self.ui.zkeySelector,vars+['color'])

    def updatePlot(self):

        # Get the chosen keys
        xkey = str(self.ui.xkeySelector.currentText())
        ykey = str(self.ui.ykeySelector.currentText())
        zkey = str(self.ui.zkeySelector.currentText())

        # Set the xrange for the plot
        dmin,dmax = self.getGlobalMinMax(xkey)
        margin = 0.1*(dmax-dmin)
        if margin == 0:
            margin = 1
        self.axes.set_xlim([dmin-margin,dmax+margin])

        # Set the yrange for the plot
        dmin,dmax = self.getGlobalMinMax(ykey)
        margin = 0.1*(dmax-dmin)
        if margin == 0:
            margin = 1
        self.axes.set_ylim([dmin-margin,dmax+margin])

        # get the z range of the data
        if zkey != 'color':
            zmin,zmax = self.getGlobalMinMax(zkey)

        # clear the axes and create a new scatter plot
        self.axes.clear()
        for name,t in self.tables.iteritems():
            color = self.ui.colorLE[name].text()
            if self.ui.dotsCB[name].checkState():
                if zkey == 'color':
                    self.axes.scatter(t[xkey],t[ykey],c=color,picker=4,label=name)
                else:
                    self.axes.scatter(t[xkey],t[ykey],c=t[zkey],vmin=zmin,vmax=zmax,picker=4,label=name)
            if self.ui.lineCB[name].checkState():
                x,y = zip(*sorted(zip(t[xkey],t[ykey])))
                self.axes.plot(x,y,c=color,picker=0)

        # set axes labels
        self.axes.set_xlabel(xkey+" ("+self.tables.values()[0]["_units"][xkey]+")")
        self.axes.set_ylabel(ykey+" ("+self.tables.values()[0]["_units"][ykey]+")")

        # update the figure
        self.fig.canvas.draw()

    def getGlobalMinMax(self,key):
        globalmin = float("+inf")
        globalmax = float("-inf")
        for t in self.tables.values():
            minn = min(t[key])
            maxx = max(t[key])
            if minn < globalmin:
                globalmin = minn
            if maxx > globalmax:
                globalmax = maxx
        return globalmin,globalmax

def main():
    import table
    import sys
    import glob
    dirname = sys.argv[1]
    tables = {}
    tables[dirname] = table.Table(dirname)
    app = QApplication(sys.argv)
    window = Plotter(tables)
    window.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
