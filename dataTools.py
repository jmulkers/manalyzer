from util import convertStr
import os
import pandas as pd
import glob

def table2df(fname):

    """ read a mumax3 table and put data in a pandas dataframe """

    fname = os.path.abspath(fname)
    dirname = os.path.dirname(fname)

    df = pd.read_table(fname)

    # remove units from column names
    df.columns = ' '.join(df.columns).split()[1::2]

    # add ovfs if the number of ovf files is equal to the number of lines
    ovfs = sorted(glob.glob( os.path.join(dirname,'m??????.ovf') ))
    if len(ovfs) == df.shape[0]:
        df['ovf'] = ovfs

    return df

def writedf(df,fname):
    with open(fname,'w') as f:
        f.write("# ")
        df.to_csv(f,sep='\t',index=None,header=None)


def readTable(filename):

    """ reads a table and put it in a list of dicts """

    filename = os.path.abspath(filename)
    dirname = os.path.dirname(filename)

    f = open(filename,'r')
    header = f.readline().split()
    lines = f.readlines()
    f.close()

    keys = header[1::2]
    units = dict(zip(keys, [u[1:-1] for u in header[2::2]]))

    data = []
    for line in lines:

        values = [convertStr(v) for v in line.split()]
        d = dict(zip(keys,values))

        d.update({'_units':units})
        d.update({'_root':dirname})
        if "cnt" in keys:
            ovffilename = os.path.join(dirname,"m%.6d.ovf"%d['cnt'])
            if os.path.isfile(ovffilename):
                d.update({'_ovffile':ovffilename})

        data.append(d)

    return data


def getColumns(data,keys):

    """ get the columns with key in keys
        rows which do not have all the keys are neglected
    """

    columns = dict( (key,[]) for key in keys )
    for row in data:
        if all( key in row.keys() for key in keys ):
            for key in keys:
                columns[key].append(row[key])
    return [columns[key] for key in keys]


def interactivePlot(data,xkey,ykey=None,zkey=None):

    """ create interactive plot of data """

    import matplotlib.pyplot as plt
    import ovf
    fig = plt.figure()

    if ykey:
        x = [d[xkey] for d in data]
        y = [d[ykey] for d in data]
        plt.xlabel(xkey)
        plt.ylabel(ykey)
    else:
        x = range(len(data))
        y = [d[xkey] for d in data]
        plt.xlabel("index")
        plt.ylabel(xkey)

    if zkey:
        z = [d[zkey] for d in data]
        if type(z[0]) == str:
            setOfValues = list(set(z))
            c = [setOfValues.index(zz) for zz in z]
        else:
            c = z
    else:
        c = len(y)*[0]

    ax = fig.add_subplot(111)
    sc = ax.scatter(x,y,c=c,picker=4)
    plt.xlim([min(x),max(x)])

    def onpick(event):
        index = event.ind[0]
        print data[index]
        o = ovf.ovf(data[index]['_ovffile'])
        o.show()

    fig.canvas.mpl_connect('pick_event', onpick)
    plt.show()

