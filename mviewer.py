from PyQt4.QtCore import *
from PyQt4.QtGui import *

import mviewer_ui

import matplotlib
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
from matplotlib.figure import Figure

from subprocess import call
import os

class Scene(QGraphicsScene):

    selectPixelSignal = pyqtSignal(int,int,int,int)

    def __init__(self, parent=None):
        QGraphicsScene.__init__(self, parent)
        self.drawing = False
        self.line = None
        self.pen = QPen(QColor('red'))
        self.pen.setWidth(1)

    def addPicture(self,picture):
        self.clear()
        self.line = None
        self.pictureWidth = picture.width()
        self.addPixmap(picture)

    def mousePressEvent(self, QMouseEvent):
        x = int(QMouseEvent.scenePos().x())
        y = int(QMouseEvent.scenePos().y())
        if self.drawing == False:
            self.xLastPicked = x
            self.yLastPicked = y
            self.drawing=True
            if self.line is not None:
                self.removeItem(self.line)
        else:
            self.line = self.addLine(self.xLastPicked,self.yLastPicked,x,y,self.pen)
            self.drawing=False
            xa = self.xLastPicked
            ya = self.pictureWidth - self.yLastPicked
            xb = x
            yb = self.pictureWidth - y
            self.selectPixelSignal.emit(xa,ya,xb,yb)


class mviewer(QWidget):

    def __init__(self, ovfobject=None, parent=None):

        QWidget.__init__(self, parent)

        # set the ui
        self.ui = mviewer_ui.Ui_Form()
        self.ui.setupUi(self)

        # create plot figure
        self.fig = Figure((5.0, 4.0))
        self.fig.set_facecolor("#f3f3f3")
        self.axes = self.fig.add_subplot(111)

        # put figure in a canvas
        self.ui.canvas = FigureCanvas(self.fig)
        self.ui.canvas.setParent(self.ui.plotFrame)
        self.ui.mpl_toolbar = NavigationToolbar(self.ui.canvas, self)
        vbox = QVBoxLayout()
        vbox.addWidget(self.ui.canvas)
        vbox.addWidget(self.ui.mpl_toolbar)
        self.ui.plotFrame.setLayout(vbox)
        self.ui.plotFrame.layout().setContentsMargins(0,0,0,0)

        # put a Scene in the graphicsview
        self.scene = Scene()
        self.scene.selectPixelSignal.connect(self.pixelClick)
        self.ui.graphicsView.setScene(self.scene)
        self.ui.graphicsView.installEventFilter(self)

        # set the ovf object
        if ovfobject is not None:
            if type(ovfobject) == str:
                import ovf
                ovfobject = ovf.ovf(ovfobject)
            self.setOVF(ovfobject)

    def setOVF(self,ovf):
        self.ovf = ovf
        self.axes.clear()
        self.fig.canvas.draw()
        self.setPicture()

    def setPicture(self):
        base = "tmpHUIOWXD"
        self.ovf.writeOVFfile(base+".ovf")
        call("~/PHD/Tools/mumax3-convert -png " + base+".ovf",shell=True)
        self.scene.addPicture(QPixmap(base+".png"))
        os.remove(base+".png")
        os.remove(base+".ovf")
        self.fitPictureInView()

    def fitPictureInView(self):
        bounds = self.scene.itemsBoundingRect()
        self.ui.graphicsView.fitInView(bounds,Qt.KeepAspectRatio)

    def pixelClick(self,xa,ya,xb,yb):
        m = zip(*self.ovf.pix2pixInterpolation2d(xa,ya,xb,yb,100))
        d = m[0]
        mx = [ c.x for c in m[1] ]
        my = [ c.y for c in m[1] ]
        mz = [ c.z for c in m[1] ]
        self.axes.clear()
        self.axes.plot(m[0],mx)
        self.axes.plot(m[0],my)
        self.axes.plot(m[0],mz)
        self.fig.canvas.draw()

    def eventFilter(self,source,event):
        if source == self.ui.graphicsView:
            if event.type() == QEvent.Resize:
                self.fitPictureInView()
        return QWidget.eventFilter(self,source,event)

def main():
    import sys
    ovffilename = sys.argv[1]
    app = QApplication(sys.argv)
    window = mviewer(ovffilename)
    window.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
