#!/bin/bash
swig -c++ -python ovf.i
g++ -std=c++11 -fPIC -c ovf.cpp ovf_wrap.cxx -I/usr/include/python2.7
g++ -shared ovf.o ovf_wrap.o -o _ovf.so
