%module ovf
%include <std_string.i>
%include <std_pair.i>
%include <std_set.i>
%include <std_map.i>
%include <std_vector.i>
%{
#include "ovf.h"
%}
%include "ovf.h"

namespace std {
   %template() std::pair<double,double>;
   %template() std::pair<double,coor>;
   %template(DoubleVector) vector<double>;
   %template(IntVector) vector<int>;
   %template(PairVector) vector< pair<double,double> >;
   %template(PairIntVector) vector< pair<int,int> >;
   %template(IntIntSet) set< pair<int,int> >;
   %template(IntIntMap) map< int,int >;
   %template(IntDoubleMap) map< int,double >;
   %template(DoubleCoorVector) vector< pair<double,coor> >;
}
%pythoncode %{

def convert(self,str,filename):
    import subprocess
    self.writeOVFfile(filename+".ovf")
    subprocess.call("~/PHD/Tools/mumax3-convert "+str+" "+filename+".ovf; rm "+filename+".ovf",shell=True)
ovf.convert = convert

def show(self):
    import subprocess
    import os
    from IPython.display import Image
    from IPython.display import display
    ovffile="temp.ovf"
    self.writeOVFfile(ovffile)
    subprocess.call("~/PHD/Tools/mumax3-convert -png " + ovffile,shell=True)
    pngfile = os.path.splitext(ovffile)[0]+".png"
    display(Image(pngfile))
    os.remove(pngfile)
    os.remove(ovffile)
ovf.show = show

def plotRadialDistribution(self,maxd,nbars,nsamples):
    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot([0,maxd],[0.5,0.5])
    plt.xlabel("d (nm)")
    plt.ylabel("p")
    x,y = zip(*self.distr(maxd,nbars,nsamples))
    ax.plot(x,y,'.-')
    plt.show()
ovf.plotRadialDistribution = plotRadialDistribution

def getNpArray(self):
    import numpy as np
    nodes=self.getNodes()
    Mx = np.array(self.getMx())
    My = np.array(self.getMy())
    Mz = np.array(self.getMz())
    Mx = Mx.reshape((nodes.z,nodes.y,nodes.x))
    My = My.reshape((nodes.z,nodes.y,nodes.x))
    Mz = Mz.reshape((nodes.z,nodes.y,nodes.x))
    return Mx,My,Mz;
ovf.getNpArray = getNpArray
%}
