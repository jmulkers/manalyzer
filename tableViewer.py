#!/usr/bin/env python

import sip
sip.setapi('QString', 2)
sip.setapi('QVariant', 2)

from plotter import Plotter
from mviewer import mviewer
from console import Console

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import sys

class TableViewer(QMainWindow):

    def __init__(self, tabulars=None, parent=None):

        QMainWindow.__init__(self, parent)

        self.tables = tabulars

        self.plotter = Plotter(self.tables)
        self.mviewer = mviewer()
        self.console = Console()

        self.dockWidgets = {}
        self.createDockWidget("Plotter",self.plotter,Qt.LeftDockWidgetArea)
        self.createDockWidget("Mviewer",self.mviewer,Qt.RightDockWidgetArea)
        self.createDockWidget("Console",self.console,Qt.BottomDockWidgetArea)

        self.console.push({'tables':self.tables})
        self.console.push({'mViewer':self.mviewer})
        self.console.push({'plotter':self.plotter})
        self.plotter.selectSignal.connect(self.pointSelected)

    def createDockWidget(self,name,widget,location):
        self.dockWidgets[name] = QDockWidget(name)
        self.dockWidgets[name].setWidget(widget)
        self.addDockWidget(location,self.dockWidgets[name])

    def pointSelected(self,name,ind):
        o = self.tables[str(name)].getOVF(ind)
        self.mviewer.setOVF(o)

    def addTable(self,name,t):
        self.tables[name] = t


def runTableViewer(tabulars=None):

    import sys
    global app

    app = QApplication.instance()
    if app is None:
        app = QApplication(sys.argv)
    viewer = TableViewer(tabulars)
    viewer.show()
    app.exit(app.exec_())

if __name__ == "__main__":
    import sys
    import table

    tables = {}
    for dirname in sys.argv[1:]:
        tables[dirname] = table.Table(dirname)
    runTableViewer(tables)
