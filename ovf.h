#ifndef OVF_H
#define OVF_H

#include<string>
#include<vector>
#include<map>
#include<fstream>
#include<set>

typedef std::vector<double> dvec;
typedef std::vector< std::pair< double,double > > ddvec;
typedef std::vector<int> ivec;
typedef std::vector< std::pair<int,int> > iivec;
typedef std::set< std::pair<int,int> > iiset;
typedef std::map<int,double> idmap;
typedef std::map<int,int> iimap;
typedef std::vector<bool> bvec;

struct coor {
    double x,y,z;
    coor(double _x, double _y, double _z) : x(_x) , y(_y), z(_z){}
    coor() : x(0.), y(0.), z(0.) {}
};

typedef std::vector< std::pair<double,coor> > dcoorvec;

struct icoor {
    int x,y,z;
    icoor(int _x, int _y, int _z) : x(_x) , y(_y), z(_z){}
    icoor() : x(0), y(0), z(0) {}
};


class ovf {

    private:
        // Datamembers
        std::string desc, title, meshtype, meshunit, valueunit;
        int N;
        double valueRangeMinMag, valueRangeMaxMag, valueMultiplier;
	coor base, stepsize, min, max;
	icoor nodes;
	dvec mx, my, mz;
	bvec shape;

        // Cache
	mutable bvec updown;
	mutable bool updownCalculated;
        mutable double tc;
        mutable bool tcCalculated;
        mutable ivec domainLabel;
        mutable idmap domainSize;
        mutable int nDomains;
        mutable int nDomainsDown;
        mutable int nDomainsUp;
        mutable bool domainsLabeled;
        mutable bool domainSizesCalculated;
        mutable bool domainTouchingChecked;
        mutable iiset touchingDomainPairs;
        mutable double mAngle;
        mutable bool mAngleCalculated;

        // Private helper functions
        void readHeader(std::ifstream& file);
        void readSegment(std::ifstream& file);
	void readDataText(std::ifstream& file);
        void readDataBinary(std::ifstream& file);
        bool setVar(const std::string  key, const std::string value);
	void setShape();
        void modified() const;

    public:

        ovf(std::string filename);
	ovf();
        void readOVFfile(const std::string filename);
        void writeOVFfile(const std::string filename) const ;

        void printInfo() const;
	double topCharge() const;
	double topChargeDensity() const;
	bool inShape(int x, int y, int z) const;
	ovf iso(double l, double u) const;
	ovf contrast() const;
	void calcUpdown() const;

	icoor getNodes() const;
	coor getMin() const;
	coor getMax() const;
	coor getStepsize() const;
	dvec getMx() const;
	dvec getMy() const;
	dvec getMz() const;
	std::vector< dvec > getM() const;

	coor position(int ix, int iy, int iz) const;
	coor position(int i) const;
	coor position(icoor I) const;

        double distance(coor pos1, coor pos2) const;
	double distance(int i1, int i2) const;

	ddvec distrRandSampling(double dmax, int nbars, int nsamples) const;
	ddvec distr(double dmax, int nbars, int nsamples) const;
        double typLength(double dmax, int nbars, int nsamples) const;

        ivec labelDomains() const;
        idmap domainSizes() const;
        int countDomains(std::string upordown = "upAndDown") const;
        double domainDensity(std::string upordown = "upAndDown") const;
        iiset touchingDomains() const;
        iimap nTouchingDomains() const;
        idmap domainWallLength() const;

	coor magnetization(int ix, int iy, int iz) const;
	coor interpolation2d(coor pos) const;
        dcoorvec pix2pixInterpolation2d(int ixa, int iya, int ixb, int iyb, int nsamples) const;

	ovf range(int ixa, int ixb, int iya, int iyb, int iza, int izb) const;
	ovf rangeDist(double xa, double xb, double ya, double yb, double za, double zb) const;

	double angle(int i, int j) const;
	double maxAngle() const;

	icoor ind2subind(int i) const;
	int subind2ind(int ix, int iy, int iz) const;
	int subind2ind(icoor i) const;

};

#endif // OVF_H
